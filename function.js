// import data
const data = require('./data');

const data1 = () => {
  const functionData = [];
  for (let i = 0; i < data.length; i++) {
    // age dibawah 30 tahun dan favorit bua pisang
    if (data[i].age < 30 && data[i].favoriteFruit === "banana") {
      functionData.push(data[i]);
    }
  }
  return functionData;
};

const data2 = () => {
  const functionData = [];
  for (let i = 0; i < data.length; i++) {
    // gender female atau company FSW4 dan age diatas 30 tahun 
    if ((data[i].gender === "female" || data[i].company === "FSW4") && data[i].age > 30) {
      functionData.push(data[i]);
    }
  }
  return functionData;
};

const data3 = () => {
  // inisialisasi array kosong
  const functionData = [];
  for (let i = 0; i < data.length; i++) {
    // warna mata biru dan age diantara 35 sampai dengan 40, dan favorit buah apel
    if (data[i].eyeColor === "blue" && (data[i].age >= 35 && data[i].age <= 40) && data[i].favoriteFruit === "apple") {
      functionData.push(data[i]);
    }
  }
  return functionData;
};

const data4 = () => {
  const functionData = [];
  for (let i = 0; i < data.length; i++) {
    // company Pelangi atau Intel, dan warna mata hijau
    if ((data[i].company === "Pelangi" || data[i].company === "Intel") && data[i].eyeColor === "green") {
      functionData.push(data[i]);
    }
  }
  return functionData;
};

const data5 = () => {
  const functionData = [];
  for (let i = 0; i < data.length; i++) {
    let year = (new Date(data[i].registered)).getFullYear();
    // registered di bawah tahun 2016 dan masih active(true)
    if (year < 2016 && data[i].isActive) {
      functionData.push(data[i]);
    }
  }
  return functionData;
};

module.exports = { data1, data2, data3, data4, data5 };