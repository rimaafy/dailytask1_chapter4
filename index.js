// import http module
const http = require('http');

// import atau panggil dotenv module (third party module)
require('dotenv').config();
const PORT = process.env.PORT;

// import data function
const { data1, data2, data3, data4, data5 } = require('./function');

// converts a JavaScript value to a JSON string
function toJSON(value) {
  return JSON.stringify(value);
}

// initialize fs and path module
const fs = require('fs');
const path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, 'public');

// path.join() method joins the specified path segments into one path
function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8');
}

// setup display css
function getCSS(cssFileName) {
  const cssPath = path.join(__dirname, 'public', 'css', cssFileName);
  return fs.readFileSync(cssPath, { encoding: 'utf-8' });
}

// request function
function onRequest(req, res) {
  // akan dipanggil ketika terdapat data
  const dataAvailable = () => {
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
  };
  // akan dipanggil ketika tidak ada data
  const dataUnavailable = () => {
    res.setHeader('Content-Type', 'application/json');
    res.end(toJSON({
      message: "data tidak ada / tidak ditemukan"
    }));
  };

  switch (req.url) {
    case "/":
      res.writeHead(200);
      res.end(getHTML("index.html"));
      return;

    case "/about":
      res.writeHead(200);
      res.end(getHTML("about.html"));
      return;

    case "/style.css":
      res.writeHead(200, { 'Content-type': 'text/css' });
      res.write(getCSS('style.css'));
      res.end();
      return;

    case "/404.png":
      const imgPath = path.join(__dirname, 'public', 'img', '404.png');
      fs.readFile(imgPath, function (err, data) {
        if (err) throw err;
        res.writeHead(200, { 'Content-type': 'image/png' });
        res.end(data);
      });
      return;

    case "/data1":
      if (data1().length < 1) {
        dataUnavailable();
      }
      dataAvailable();
      res.end(toJSON(data1()));
      return;

    case "/data2":
      if (data2().length < 1) {
        dataUnavailable();
      }
      dataAvailable();
      res.end(toJSON(data2()));
      return;

    case "/data3":
      if (data3().length < 1) {
        dataUnavailable();
      }
      dataAvailable();
      res.end(toJSON(data3()));
      return;

    case "/data4":
      if (data4().length < 1) {
        dataUnavailable();
      }
      dataAvailable();
      res.end(toJSON(data4()));
      return;

    case "/data5":
      if (data5().length < 1) {
        dataUnavailable();
      }
      dataAvailable();
      res.end(toJSON(data5()));
      return;

    default:
      res.writeHead(404);
      res.end(getHTML("404.html"));
      return;
  }
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT,'0.0.0.0', () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})